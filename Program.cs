﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

namespace graph_parsing
{
    class Program
    {
        private readonly static string RootDirectoryPath = Path.Combine(Assembly.GetExecutingAssembly().Location, @"../../../../");
        private const string GraphsDirectoryPath = @"graphs";        
        static void Main(string[] args)
        {            
            var loop = true;
            while (loop)
            {
                Console.WriteLine("Please select:\n1) Test all\n2) Test specific\nPress e to escape");
                var selection = Console.ReadLine().ToLowerInvariant();
                switch (selection)
                {
                    case "1": LoadAll();
                        break;
                    case "2": LoadSpecific();
                        break;
                    case "e": Console.WriteLine("Game over");
                        loop = false;
                        break;
                    default:
                        Console.WriteLine("Invalid selection");
                        break;
                }
            }
        }

        private static void LoadSpecific()
        {
            var paths = GetPaths().ToArray(); //Inefficient?
            PrintFilenames(paths);
            Console.WriteLine("Which file would you like to load?");
            var selection = Convert.ToInt32(Console.ReadLine());
            if (selection >= 0 && selection < paths.Count() && TryGetGraph(paths[selection], out var graph))
            {
                TestGraph(graph);
            }

        }

        private static void LoadAll()
        {
            var graphPaths =  GetPaths();

            foreach (var path in graphPaths) 
            {
                if (Path.GetExtension(path) != ".graph") continue;

                Console.WriteLine($"Path: {Path.GetFileName(path)}");

                if (!TryGetGraph(path, out var graph)) continue;

                TestGraph(graph);
            }
        }

        private static void TestGraph(Graph graph)
        {
            Console.WriteLine($"Data: Is colour two? {graph.IsColour2}");
            Console.WriteLine($"Test: Is colour two? {graph.IsColourTwo()}");
        }

        private static string GetGraphDirectory()
        {
            return Path.Combine(RootDirectoryPath, GraphsDirectoryPath);
        }

        private static IEnumerable<string> GetPaths()
        {
            return Directory.GetFiles(GetGraphDirectory()).Where(p => Path.GetExtension(p) == ".graph");
        }

        private static void PrintFilenames(string[] paths)
        {
            foreach (var path in paths)
            {
                Console.WriteLine($"{Array.IndexOf(paths, path)}: {Path.GetFileName(path)}");
            }
        }
        private static bool TryGetGraph(string filePath, out Graph graph)
        {
            var lines = File.ReadAllLines(filePath);
            
            if (!lines.Any())
            {
                graph = null;
                return false;
            }

            var settings = lines.Last().Split(' ');

            if (settings.Count() < 2 || !bool.TryParse(settings[1], out var isColour2))
            {
                Console.WriteLine($"FAILED: {filePath}");
                graph = null;
                return false;
            }

            graph = new Graph(Path.GetFileName(filePath), isColour2);
            var counts = lines[0].Split(' ');

            if (counts.Count() < 2 || !int.TryParse(counts[0], out var nodeCount) || !int.TryParse(counts[1], out var edgeCount))
            {
                Console.WriteLine($"FAILED: {Path.GetFileName(filePath)}");
                graph = null;
                return false;
            }

            // Add nodes
            for (var i = 1; i < nodeCount + 1; i++)
            {
                var node = new Node(lines[i]);

                Console.WriteLine($"Node: {node.Name}");

                graph.Nodes.Add(node);
            }

            // Add edges
            for (var i = nodeCount; i < (edgeCount + nodeCount + 1); i++)
            {
                var nodes = lines[i].Split(' ');

                if (nodes.Count() < 2) continue;

                if (!graph.TryGetNode(nodes[0], out var node1)) continue;
                if (!graph.TryGetNode(nodes[1], out var node2)) continue;

                Console.WriteLine($"Edge: {node1.Name}, {node2.Name}");

                graph.Edges.Add(new Edge(node1, node2));
            }
            
            Console.WriteLine($"Settings: {settings[0]}, {isColour2}");
            return true;
        }
    }
}
