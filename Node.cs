namespace graph_parsing
{
    public class Node
    {
        public readonly string Name;

        public Node(string name)
        {
            this.Name = name;
        }

        public override string ToString()
        {
            return $"Name: {this.Name}";
        }
    }
}