using System.Collections.Generic;
using System.Linq;

namespace graph_parsing
{
    public class Graph
    {
        public readonly List<Node> Nodes = new List<Node>();
        public readonly List<Edge> Edges = new List<Edge>();
        public readonly string Filename;
        public readonly bool IsColour2;

        public Graph(string filename, bool isColour2)
        {
            this.Filename = filename;
            this.IsColour2 = isColour2;
        }

        public override string ToString()
        {
            return $"File: {this.Filename}, Nodes: {this.Nodes.Count}, Edges: {this.Edges.Count}";
        }
        public bool TryGetNode(string name, out Node node)
        {
            if (!this.Nodes.Any(n => n.Name == name)){
                node = null;
                return false;
            }

            node = this.Nodes.Single(n => n.Name == name);
            return true;
        }

        public HashSet<Node> GetConnectedNodes(Node node)
        {            
            var result = new HashSet<Node>();
            foreach (var edge in this.Edges) 
            {
                if (edge.Node1 == node)
                {
                    result.Add(edge.Node2);
                }
                else if (edge.Node2 == node)
                {
                    result.Add(edge.Node1);
                }
            }
            return result;
        }

        public bool IsColourTwo()
        {
            if (!this.Nodes.Any()) return true;
            if (this.Nodes.Count() == 1) return true;

            foreach (var node in this.Nodes)
            {
                if (this.HasOddConnectedCircuit(node))
                {
                    return false;
                }
            }

            return true;
        }

        private bool HasOddConnectedCircuit(Node root)
        {
            // Get all connections to our root node 
            var testStack = new Stack<(Node parent, Node child)>();

            // Add connections to test stack with parent identified
            foreach (var item in this.GetConnectedNodes(root))
            {
                testStack.Push((root, item));
            }

            var path = new Stack<Node>();
            path.Push(root);
            //var parent = root;

            // Loop over every connection and start building paths
            while (testStack.Any())
            {
                var nodePair = testStack.Pop();
                var connections = this.GetConnectedNodes(nodePair.child);
                connections.Remove(nodePair.parent);

                // If we hit a duplicate but it's not our root we've found 
                // another circuit but not the one we're looking at, ignore.
                // Not sure if we can get to this stage now? Might not need.
                if (path.Contains(nodePair.child) && nodePair.child != root)
                {
                    continue;
                }
                
                // If the node we're about to add to our path matches our 
                // root node and our path has an odd number of vertices we've 
                // got a bad circuit
                if (nodePair.child == root && (path.Count() % 2) != 0) 
                {
                    return true;
                }

                // If we've found a match but the path is even we're
                // all sweet, this path is finished we can keep moving
                if (nodePair.child == root && (path.Count() % 2) == 0) 
                {
                    path.Clear();
                    path.Push(root);
                    continue;
                }

                path.Push(nodePair.child);

                // If the path with the addition of this next node is odd
                // number and any of it's connecting nodes lead back to 
                // our root, then we've got a bad circuit.
                if ((path.Count() % 2) != 0 && connections.Contains(root))
                {
                    return true;
                }

                // If we haven't hit a circuit on this round we need to 
                // add these connections to the test stack so we can dive 
                // deeper.
                foreach (var item in connections)
                {
                    testStack.Push((nodePair.child, item));
                }
            }

            return false;
        }
    }
}