-- FORMAT --

The first line of a file is:

	x y

Where 'x' represents the number of nodes in a graph, and 'y' represents the number of edges.

The next 'x' lines have the form:

	NODE_ID

These lines represent each node in the graph, with the first word (sequence of non-whitespace characters) of each line being the id of the node.

The next 'y' lines have the form:

	NODE1_ID NODE2_ID
	
These lines represent each edge in the graph, with the first and second word being the ids of the connected nodes

Every line afterwards has the form:

	NAME RESULT

These lines represent the expected result of a function on this graph.

-- EXAMPLE --

4 3
alpha
beta
charlie
delta
alpha charlie
beta charlie
delta charlie
colour2 true

This file represents the graph:

alpha     beta
  |      /
  |     /
  |    /
charlie --- delta

And when this graph is passed to the 'colour2' function, the expected result is 'true'


