namespace graph_parsing
{
    public class Edge
    {
        public readonly Node Node1;
        public readonly Node Node2;

        public Edge(Node node1, Node node2)
        {
            this.Node1 = node1;
            this.Node2 = node2;
        }

        public override string ToString()
        {
            return $"Node1: {this.Node1}, Node2: {this.Node2}";
        }
    }
}